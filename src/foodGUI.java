import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class foodGUI {

    private JPanel root;
    private JButton maguroButton;
    private JButton beerButton;
    private JTextPane orderedItemsList;
    private JButton aziButton;
    private JButton buriButton;
    private JButton uLonTeaButton;
    private JButton checkOutButton;
    private JLabel TotalYen;
    private JButton iceCreamButton;

    public int total = 0;

    int price(String food){
        int price = 0;
        switch (food){
            case "maguro":
                price += 450;
                break;
            case "azi":
                price += 250;
                break;
            case "buri":
                price += 300;
                break;
            case "u-lon-tea":
                price +=200;
                break;
            case "beer":
                price += 350;
                break;
            case "iceCream":
                price += 150;
            default:
                break;
        }
        return price;
    }

    void order(String food){
        int confirmbeer = 0;
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ food+"?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(food=="beer"){
            confirmbeer = JOptionPane.showConfirmDialog(
                    null,
                    "Are you older than 20 years old?",
                    "Age confirmation",
                    JOptionPane.YES_NO_OPTION
            );
        }

        if(confirmation == 0 && confirmbeer == 0){
            String orderMessage = String.format("Thank you for ordering %s! It will be served as soon as possible.",food);
            total += price(food);
            TotalYen.setText("Total " + total + " yen");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + " " + price(food) +  "yen" + "\n");
            JOptionPane.showMessageDialog(null,orderMessage);
        }
    }

    void confirm(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation ==0){
            String ThankMessage = String.format("Thank you. The total price is %d yen.",total);
            JOptionPane.showMessageDialog(null,ThankMessage);
            orderedItemsList.setText("");
            total = 0;
            TotalYen.setText("Total " + total + " yen");
        }
    }



    public  foodGUI(){
        maguroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("maguro");
            }
        });
        aziButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("azi");
            }
        });
        buriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("buri");
            }
        });
        uLonTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("u-lon-tea");
            }
        });
        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("beer");
            }
        });
        iceCreamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("iceCream");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                confirm();
            }
        });



        maguroButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/maguro.jpg")
        ));
        aziButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/azi_fish.jpeg")
        ));
        buriButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/buri.jpg")
        ));
        uLonTeaButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/u-lon-tea.jpeg")
        ));
        beerButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/beer.jpeg")
        ));
        iceCreamButton.setIcon(new ImageIcon(
                this.getClass().getResource("./images/ice.jpg")
        ));


    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new foodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
